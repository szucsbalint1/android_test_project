package com.gitlab.rockwotj.testingexample;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

/**
 * A simple {@link Activity} that shows a message.
 */
public class ShowTextActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_text);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Get the meesage from the intent.
        Intent intent = getIntent();
        String message = intent.getStringExtra("data");

        // Show message.
        ((TextView)findViewById(R.id.text_view)).setText(message);

        // Setup FAB to go back to previous activity.
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        // Add back arrow to toolbar.
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    /**
     * Creates an {@link Intent} for {@link ShowTextActivity} with the message to be displayed.
     * @param context the {@link Context} where the {@link Intent} will be used
     * @param text a {@link String} with text to be displayed
     * @return an {@link Intent} used to start {@link ShowTextActivity}
     */
    public static Intent newStartIntent(Context context, String text) {
        Intent intent = new Intent(context, ShowTextActivity.class);
        intent.putExtra("data", text);
        return intent;
    }
}
